#!/bin/bash

set -m

# RUN start script
if [ -f /server/package.json ]; then
  echo cd /server npm install
  cd /server
  npm install && npm run start
fi

tail -f /dev/null

fg
