# Nodejs Docker Image

## build it
```bash
docker build -t wbeckervid/nodejsgcloud:1.0.0 .
```

## run it
```bash

# run the Image
docker run -i -d \
-p 2700:2700 \
-p 2800:2800 \
-v /Users/wolfgangbecker/Documents/PROJECTS_LC/VID-IO-SERVER/out/:/server  \
-v /Users/wolfgangbecker/.config/:/root/.config  \
--name io-server \
wbeckervid/nodejsgcloud:1.0.0

```

## log into container
```bash

# log into container
docker exec -t -i io-server /bin/bash

# cd to src dir
cd /server
npm install

#test
node server.js
```

## create a Javascript launch script server.js

## start app

#### start via node
`node server.js`

#### start via forever
```bash
forever /server/server.js
```
